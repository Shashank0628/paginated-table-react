import {useState} from 'react';
import './App.css';
import {TableProvider} from './components/Table/TableContext';
import {PageProvider} from './PageContext';
import {PageSizeProvider} from './PageSizeContext';
import Table from './components/Table/Table';
import TableBar from './components/TableBar/TableBar';
function App() {

  const [page , setPage] = useState(1)
  const [pageSize , setPageSize] = useState(5)

  return (
    
    <PageProvider>
      <PageSizeProvider>
        <TableProvider>
          <div className="App">
            <h1>Paginated Table</h1>
              <Table />
              <TableBar />
          </div>
        </TableProvider>
      </PageSizeProvider>
   </PageProvider>
  );
}

export default App;
