import React , {useState , createContext} from 'react';

export const PageSizeContext = createContext();

export const PageSizeProvider = (props) => {
    let [pageSize , setPageSize] = useState(5)
    return (
        <PageSizeContext.Provider value = {[pageSize, setPageSize]} >
            {props.children}
        </PageSizeContext.Provider>
    )
}