import React , {useState , useEffect , useContext} from 'react';
import {TableContext} from './TableContext';
import style from './Table.module.css';
import {PageContext} from '../../PageContext';
import {PageSizeContext} from '../../PageSizeContext';

const Table = () => {
    const [students , setStudents] = useContext(TableContext)
    const [page , setPage] = useContext(PageContext)
    const [pageSize , setPageSize] = useContext(PageSizeContext)
    let [paginatedItems , setPaginatedItems] = useState([])

    
    const sliceItems = () => {
        let current = page;
        let rows_per_page = pageSize;
        current--;
        let startloop = current*rows_per_page;
        let endloop = startloop + rows_per_page;
        setPaginatedItems(students.slice(startloop , endloop))
    }

    useEffect(()=>{
        sliceItems();
    } , [page , pageSize])

    return (
        <div>
             <table className = {style.pgtable}>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Roll</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {paginatedItems.map(student=>(
                        <tr key = {student.name}>
                            <td>{student.name}</td>
                            <td>{student.rollNo}</td>
                            <td>{student.email}</td>
                        </tr>
                    ))}
                </tbody>
             </table>
        </div>
       
    )
}

export default Table;