import React , {useState , createContext} from 'react';


export const TableContext = createContext();

export const TableProvider = (props) => {
    const [students , setStudents] = useState([{name: "Item1 " , rollNo: "101" , email: "item1.gmail.com"},
                                         {name: "Item2 " , rollNo: "102" , email: "item2.gmail.com"},   
                                         {name: "Item3 " , rollNo: "103" , email: "item3.gmail.com"},
                                         {name: "Item4 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item5 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item6 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item7 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item8 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item9 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item10 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item11 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item12 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item13 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item14 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item15 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item16 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item17 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item18 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item19 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item20 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item21 " , rollNo: "103" , email: "item3.gmail.com"},  
                                         {name: "Item22 " , rollNo: "104" , email: "item4.gmail.com"}   ])
    return (
        <TableContext.Provider value = {[students,setStudents]}>
            {props.children}
        </TableContext.Provider>
    )                                

}