import React , {useState , useEffect , useContext} from 'react';
import style from '../TableBar.module.css';
import {TableContext} from '../../Table/TableContext';
import {PageContext} from '../../../PageContext';
import {PageSizeContext} from '../../../PageSizeContext';

const Buttons = ()=>{
    let [button , setButtons] = useState([])
    const [students , setStudents] = useContext(TableContext)
    const [page , setPage] = useContext(PageContext)
    const [pageSize , setPageSize] = useContext(PageSizeContext)
    
    const setupPagination = ()=>{
        document.querySelector('#buttons').innerHTML = ''
        let no_of_pages = Math.ceil(students.length/pageSize);
        for(let i = 1 ; i <= no_of_pages ; i++){
            setButtons(arr => {
                return [...arr, i]
            });
        }
        
    }

    useEffect(() => { 
        setupPagination()
    } , [pageSize])

    return (
                <div id = "buttons" className = {style.buttons}>
                {
                    button.map(button => {
                                    if(button == page){
                                        return <button key = {button} className = {style.bluebuttons + " " + style.active}>{button}</button>
                                    }
                                    else{
                                        return <button key = {button} className = {style.bluebuttons}>{button}</button>
                                    }
                                 }
                            )
                }
                </div>
    )
}

export default Buttons;