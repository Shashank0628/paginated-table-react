import React , {useState , useEffect , useContext} from 'react';
import style from '../TableBar.module.css';
import {TableContext} from '../../Table/TableContext';
import {PageContext} from '../../../PageContext';
import {PageSizeContext} from '../../../PageSizeContext';

const Navigation = ()=>{
    
    const [students , setStudents] = useContext(TableContext)
    let [page , setPage] = useContext(PageContext)
    const [pageSize , setPageSize] = useContext(PageSizeContext)
    
    const nextPage = () => { 
        const lastPage = Math.ceil(students.length/pageSize)
        if(page < lastPage){
            setPage(page+1)
        }
    }

    const prevPage = () => {
        if(page > 1){
            setPage(page-1)
        }
    }

    const firstPage = () => {
        setPage(1)
    }

    const lastPage = () => {
        const lastPage = Math.ceil(students.length/pageSize)
        setPage(lastPage)
    }
    return (
        <div id = 'navigation' className = {style.navigation}> 
                <button className={style.bluebuttons} onClick = {firstPage}>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-140.96875 0-256 115.050781-256 256 0 140.96875 115.050781 256 256 256 140.96875 0 256-115.050781 256-256 0-140.96875-115.050781-256-256-256zm0 482c-124.617188 0-226-101.382812-226-226s101.382812-226 226-226 226 101.382812 226 226-101.382812 226-226 226zm0 0"/><path d="m256 136-160 120 160 120v-90l120 90v-240l-120 90zm90 60v120l-80-60zm-120 120-80-60 80-60zm0 0"/></svg>
                </button>

                <button className={style.bluebuttons} onClick={prevPage}>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-140.96875 0-256 115.050781-256 256 0 140.96875 115.050781 256 256 256 140.96875 0 256-115.050781 256-256 0-140.96875-115.050781-256-256-256zm0 482c-124.617188 0-226-101.382812-226-226s101.382812-226 226-226 226 101.382812 226 226-101.382812 226-226 226zm0 0"/><path d="m256 376v-240l-160 120zm-30-60-80-60 80-60zm0 0"/><path d="m286 361h90v-210h-90zm30-180h30v150h-30zm0 0"/></svg>
                </button>


                <span id='page-number'>
                    {page}
                </span> &nbsp;&nbsp;

                <button className={style.bluebuttons} onClick={nextPage}>
                    <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-140.96875 0-256 115.050781-256 256 0 140.972656 115.050781 256 256 256 140.96875 0 256-115.050781 256-256 0-140.972656-115.050781-256-256-256zm0 482c-124.617188 0-226-101.382812-226-226s101.382812-226 226-226 226 101.382812 226 226-101.382812 226-226 226zm0 0"/><path d="m256 136v240l160-120zm30 60 80 60-80 60zm0 0"/><path d="m136 361h90v-210h-90zm30-180h30v150h-30zm0 0"/></svg>
                </button>
                

                <button className={style.bluebuttons} onClick = {lastPage}>
                    <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-140.96875 0-256 115.050781-256 256 0 140.972656 115.050781 256 256 256 140.96875 0 256-115.050781 256-256 0-140.972656-115.050781-256-256-256zm0 482c-124.617188 0-226-101.382812-226-226s101.382812-226 226-226 226 101.382812 226 226-101.382812 226-226 226zm0 0"/><path d="m256 136v90l-120-90v240l120-90v90l160-120zm-90 180v-120l80 60zm120-120 80 60-80 60zm0 0"/></svg>
                </button>
        </div>
    )
}

export default Navigation;