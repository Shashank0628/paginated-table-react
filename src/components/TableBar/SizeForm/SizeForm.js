import React , {useContext,useState , useEffect} from 'react';
import style from '../TableBar.module.css';
import {PageSizeContext} from '../../../PageSizeContext';

const SizeForm = () => {
    const [pageSize , setPageSize] = useContext(PageSizeContext)

    const submitForm = e => {
        e.preventDefault()
        const rows_per_page = e.target.elements.pagesize.value
        setPageSize(rows_per_page)
    }

    return (
        <form action="#" onSubmit = {submitForm} className = {style.sizeform}>
            <select name="pagesize" id="" className = {style.pageselect}>
                <option value="5"> 5 </option>
                <option value="10"> 10 </option>
                <option value="15"> 15 </option>
                <option value="20"> 20 </option>
            </select>
            <button className={style.formbutton}>
                Submit
            </button>
        </form>
    )
}


export default SizeForm;