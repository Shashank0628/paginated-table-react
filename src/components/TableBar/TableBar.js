import React from 'react';
import style from './TableBar.module.css';
import Buttons from './Buttons/Buttons';
import Navigation from './Navigation/Navigation';
import SizeForm from './SizeForm/SizeForm';

const TableBar = () => {
    return (
        <div className = {style.main} >
           <Navigation />
           <Buttons />
           <SizeForm />
        </div>
    )
}

export default TableBar;